@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('My page.') }}</div>

                <div class="card-body">
                    <p>Name: {{$user->name}}</p>
                    <p>Email: {{$user->email}}</p>
                </div>
            </div>
        </div>
    </div>

    @if(Auth::User()->is_admin == true)
        <div class="mt-3">
            <h3>News without publication date</h3>
            <ul>
                @foreach($articles as $article)
                    <li>
                        <a style="font-size: 20px; color: red" href="{{route('articles.show', ['article' => $article])}}">{{$article->title}}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="row p-5">
            <div class="col-12">
                {{ $articles->links() }}
            </div>
        </div>
    @endif

@endsection
