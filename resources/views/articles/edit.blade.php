@extends('layouts.app')

@section('content')

    <div>
        <h2>Editing News</h2>
    </div>

    <form class="w-75 mt-5 mb-5" method="post" action="{{route('articles.update', ['article' => $article])}}" >
        @csrf
        @method('put')

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{$article->title}}">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">Text</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{$article->body}}</textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="category_id">Category</label>
            <select class="form-control w-25 @error('category_id') is-invalid @enderror" id="category_id" name="category_id">
                @foreach($categories as $category)
                    <option value="{{$category->id}}" @if($category->id == $article->category->id) selected @endif>{{$category->title}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="tag_id">Tag</label>
            <select class="form-control w-25 @error('tag_id') is-invalid @enderror" id="tag_id" name="tag_id">
                @foreach($tags as $tag)
                    <option value="{{$tag->id}}" @if($tag->id == $article->tag->id) selected @endif>{{$tag->title}}</option>
                @endforeach
            </select>
        </div>

        @can('view', $article)
            <div class="form-group">
                <label for="publication_date">Publication date</label>
                <input type="date" class="form-control w-25 @error('publication_date') is-invalid @enderror" id="publication_date" name="publication_date" value="{{$article->publication_date}}">
                @error('publication_date')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>
        @endcan

        <button type="submit" class="btn btn-primary">Edit</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('articles.show', ['article' => $article])}}">Back</a>
    </div>

@endsection
