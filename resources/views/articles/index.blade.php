@extends('layouts.app')

@section('content')

    <div class="text-center">
        <h2>All News</h2>
    </div>

    <div class="text-right">
        <h3><a href="{{route('articles.create')}}" class="text-success">Create News</a></h3>
    </div>

    <div>
        <ul>
            @foreach($articles as $article)
                <li>
                    <a style="font-size: 20px" href="{{route('articles.show', ['article' => $article])}}">{{$article->title}}</a>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="row p-5">
        <div class="col-12">
            {{ $articles->links() }}
        </div>
    </div>

@endsection
