@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            {{$article->title}}
        </div>
        <div class="card-body">
            <p class="card-text text-right text-muted">Author: <a href="">{{$article->user->name}}</a></p>
            <p class="card-text">{{$article->body}}</p>
            <p class="card-text text-muted">Category: <small><a href="">{{$article->category->title}}</a></small></p>
            <p class="card-text text-muted">Tag: <small><a href="">#{{$article->tag->title}}</a></small></p>
            <p class="card-text text-muted text-right">Create: <small>{{$article->created_at->diffForHumans()}}</small></p>
            <p class="card-text text-muted text-right">Publication date: <small>{{$article->publication_date}}</small></p>
        </div>
    </div>

    <div class="text-right">
        @can('update', $article)
            <a href="{{route('articles.edit', ['article' => $article])}}" class="btn btn-success">Edit News</a>
        @endcan

        @can('delete', $article)
            <form class="mt-3" method="post" action="{{route('articles.destroy', ['article' => $article])}}">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-outline-danger btn-sm">Delete News</button>
            </form>
        @endcan
    </div>

    <div>
        <h3>News score</h3>

        <form method="post" action="{{route('ratings.store')}}" class="w-25 mb-3">
            @csrf
            <input type="hidden" id="article_id" name="article_id" value="{{$article->id}}">

            <div class="form-group">
                <label for="quality">Quality</label>
                <select class="form-control" id="quality" name="quality">
                    <option value="1">Qualitatively</option>
                    <option value="-1">Poor-quality</option>
                </select>
            </div>

            <div class="form-group">
                <label for="relevance">Relevance</label>
                <select class="form-control" id="relevance" name="relevance">
                    <option value="1">Actual</option>
                    <option value="-1">Not actual</option>
                </select>
            </div>

            <div class="form-group">
                <label for="satisfied">Satisfied</label>
                <select class="form-control" id="satisfied" name="satisfied">
                    <option value="1">Satisfied</option>
                    <option value="-1">Dissatisfied</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Add feedback</button>
        </form>
    </div>

    <div>
        <button class="show-comments-btn btn btn-info mt-4" type="button" data-toggle="collapse"
                data-target="#comments-block">@lang('Comments'): <span class="comments-number">{{$comments_count}}</span></button>
        <div id="comments-block" class=" collapse multi-collapse mt-5">
            @foreach($article->comments as $comment)
                <div class="card mb-3 w-50">
                    <h5 class="card-header">{{$comment->user->name}}</h5>
                    <div class="card-body">
                        <p class="card-text">{{$comment->body}}</p>
                        <p class="card-text text-right"><small class="text-muted">{{$comment->created_at->diffForHumans()}}</small></p>
                        <div>
                            @can('update', $comment)
                                <hr>
                                <p><a href="{{route('comments.edit', ['comment' => $comment])}}">Edit comment</a></p>
                            @endcan
                            @can('delete', $comment)
                                <form class="mt-3" method="post" action="{{route('comments.destroy', ['comment' => $comment])}}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <form method="post" action="{{route('comments.store')}}" class="w-25 mt-5 mb-5">
        @csrf
        <input type="hidden" id="article_id" name="article_id" value="{{$article->id}}">

        <div class="form-group">
            <label for="body">Comment:</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3"></textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Add comment</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('articles.index')}}">Back</a>
    </div>

@endsection
