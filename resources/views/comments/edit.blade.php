@extends('layouts.app')

@section('content')

    <h2>Edit comment</h2>

    <form method="post" action="{{route('comments.update', ['comment' => $comment])}}" class="w-25 mt-5 mb-5">
        @csrf
        @method('put')
        <input type="hidden" id="article_id" name="article_id" value="{{$comment->article->id}}">

        <div class="form-group">
            <label for="body">Comment:</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{$comment->body}}</textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Edit comment</button>
    </form>

    <div class="mt-3 mb-5">
        <a href="{{route('articles.show', ['article' => $comment->article])}}">Back</a>
    </div>

@endsection
