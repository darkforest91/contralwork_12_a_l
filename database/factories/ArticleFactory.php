<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(rand(20, 100)),
        'body' => $faker->text(rand(300, 1000)),
        'user_id' => rand(1, 21),
        'category_id' => rand(1, 4),
        'tag_id' => rand(1, 8),
    ];
});
