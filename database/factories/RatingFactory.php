<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rating;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'quality' => rand(-1, 1),
        'relevance' => rand(-1, 1),
        'satisfied' => rand(-1, 1),
        'user_id' => rand(1, 21),
        'article_id' => rand(1, 24)
    ];
});
