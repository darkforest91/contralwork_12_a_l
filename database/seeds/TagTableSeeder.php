<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'title' => 'Auto sport'
        ]);

        DB::table('tags')->insert([
            'title' => 'Boxing'
        ]);

        DB::table('tags')->insert([
            'title' => 'Parliament'
        ]);

        DB::table('tags')->insert([
            'title' => 'Ministry'
        ]);

        DB::table('tags')->insert([
            'title' => 'Gold'
        ]);

        DB::table('tags')->insert([
            'title' => 'Dollar rate'
        ]);

        DB::table('tags')->insert([
            'title' => 'Earthquakes'
        ]);

        DB::table('tags')->insert([
            'title' => 'Fires'
        ]);
    }
}
