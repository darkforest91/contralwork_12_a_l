<?php

use App\Article;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            'title' => 'Тихановская призвала ЕС к расширению санкций против бизнеса, поддерживающего Лукашенко',
            'body' => 'Лидер белорусской оппозиции Светлана Тихановская призвала ЕС к расширению санкций против бизнеса,
                        поддерживающего президента страны Александра Лукашенко. Об этом сообщает «Интерфакс».
                        «Индивидуальные санкции против граждан Беларуси, причастных к фальсификациям на выборах и
                        политическим репрессиям, стали важным первым шагом ЕС. Этот список следует расширить и рассмотреть
                        вопрос о санкциях против бизнеса, поддерживающего режим», — заявила Тихановская.',
            'user_id' => rand(1, 21),
            'category_id' => 1,
            'tag_id' => 4,
            'publication_date' => Carbon::today(),
            'created_at' => now()
        ]);

        DB::table('articles')->insert([
            'title' => 'Названы не болеющие тяжелой формой коронавируса',
            'body' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                        sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                        Aldus PageMaker including versions of Lorem Ipsum.',
            'user_id' => rand(1, 21),
            'category_id' => rand(1, 4),
            'tag_id' => rand(1, 8),
            'publication_date' => Carbon::today(),
            'created_at' => now()
        ]);

        DB::table('articles')->insert([
            'title' => 'Бузову ударил лицом об пол партнер на проекте «Ледниковый период»',
            'body' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            'user_id' => rand(1, 21),
            'category_id' => rand(1, 4),
            'tag_id' => rand(1, 8),
            'publication_date' => Carbon::today(),
            'created_at' => now()
        ]);

        DB::table('articles')->insert([
            'title' => 'Жители перешедших Азербайджану районов Карабаха стали сжигать свои дома',
            'body' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            'user_id' => rand(1, 21),
            'category_id' => rand(1, 4),
            'tag_id' => rand(1, 8),
            'publication_date' => Carbon::today(),
            'created_at' => now()
        ]);

        DB::table('articles')->insert([
            'title' => 'Россиян предостерегли от покупки доллара',
            'body' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer
                        took a galley of type and scrambled it to make a type specimen book.',
            'user_id' => rand(1, 21),
            'category_id' => rand(1, 4),
            'tag_id' => rand(1, 8),
            'publication_date' => Carbon::today(),
            'created_at' => now()
        ]);
        factory(Article::class, 20)->create();
    }
}
