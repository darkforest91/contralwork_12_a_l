<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Politics'
        ]);

        DB::table('categories')->insert([
            'title' => 'Sport'
        ]);

        DB::table('categories')->insert([
            'title' => 'Economy'
        ]);

        DB::table('categories')->insert([
            'title' => 'Incidents'
        ]);
    }
}
