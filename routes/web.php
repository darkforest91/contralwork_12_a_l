<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticlesController@index');

Route::resource('articles', 'ArticlesController');
Route::resource('comments', 'CommentsController')->only(['store', 'edit', 'update', 'destroy']);
Route::resource('ratings', 'RatingsController')->only(['store']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
