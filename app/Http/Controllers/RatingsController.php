<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;

class RatingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $rating = new Rating($request->all());
        $rating->user_id = $request->user()->id;
        $rating->article_id = $request->input('article_id');
        $rating->save();
        $article = $request->input('article_id');

        return redirect(route('articles.show', compact('article')))->with('status', 'You rated the news');
    }
}
